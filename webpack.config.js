const webpack = require('webpack');
const path = require('path');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const SRC_DIR = path.resolve(__dirname, 'src/');
const COMPILED_DIR = path.resolve(__dirname, 'compiled/');

const config = {
    entry: [
        'webpack/hot/only-dev-server',
        SRC_DIR + '/main.jsx'
    ],
    output: {
        path: COMPILED_DIR,
        filename: 'main.js'
    },
    devtool: 'source-map',
    plugins: [
        new ExtractTextWebpackPlugin('style.css', {
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            template: SRC_DIR + '/index.html',
            inject: 'body',
            filename: 'index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /.jsx/,
                loader: 'eslint-loader',
                exclude: [/node_modules/]
            },
            {
                test: /\.jsx?$/,   // .js .jsx
                include: SRC_DIR,
                exclude: [/node_modules/],
                loaders: ['babel-loader']
            },
            {
                test: /\.scss$/,
                loader: ExtractTextWebpackPlugin.extract('css!sass')
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader: 'file-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};

module.exports = config;